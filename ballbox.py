#!/usr/bin/env python2

from visual import *
from random import *

## Constants

## objects

balls = []
for i in range(8):
    balls.append(sphere(pos=(uniform(-5, 5), uniform(-5, 5), uniform(-5, 5)), radius=0.5, color=color.cyan))
    #balls.append(sphere(pos=(i-3, 0, 0), radius=0.5, color=color.cyan))
    
wallR = box(pos=(6, 0, 0), size=(0.2, 12, 12), color=color.green)
wallL = box(pos=(-6, 0, 0), size=(0.2, 12, 12), color=color.green)
wallD = box(pos=(0, -6, 0), size=(12, 0.2, 12), color=color.green)
wallT = box(pos=(0, 6, 0), size=(12, 0.2, 12), color=color.green)
wallB = box(pos=(0, 0, -6), size=(12, 12, 0.2), color=color.green)

vscale=0.1

## initial values
for ball in balls:
    ball.velocity = vector(1, 3, uniform(-25, 25))
    ball.arr = arrow(pos=ball.pos, axis=vscale*ball.velocity)
deltat = 0.005
t = 0

#balls[-1].velocity = vector(15, 0, 0)

#varr = arrow(pos=ball.pos, axis=vscale * ball.velocity, color=color.yellow)
scene.autoscale = 0
## Simulations
while True:
    for ball in balls:
        if abs(ball.pos.x - wallR.pos.x) < ball.radius:
            ball.velocity.x = -ball.velocity.x
        if abs(ball.pos.x - wallL.pos.x) < ball.radius:
            ball.velocity.x = -ball.velocity.x
        if abs(ball.pos.y - wallT.pos.y) < ball.radius:
            ball.velocity.y = -ball.velocity.y
        if abs(ball.pos.y - wallD.pos.y) < ball.radius:
            ball.velocity.y = -ball.velocity.y
        if abs(ball.pos.z - wallB.pos.z) < ball.radius:
            ball.velocity.z = -ball.velocity.z
        if abs(ball.pos.z - 6) < ball.radius:
            ball.velocity.z = -ball.velocity.z
        ball.pos = ball.pos + ball.velocity*deltat
        if abs(ball.velocity) < 0.0001:
            ball.color = color.yellow
        else:
            ball.color = color.cyan
    for i in range(len(balls)):
        a = balls[i]
        a.color = color.cyan
        for j in range(i):
            b = balls[j]
            if abs(a.pos - b.pos) < 1.5 * abs(a.radius + b.radius):
                a.color = color.red
                b.color = color.red
            if abs(a.pos - b.pos) < abs(a.radius + b.radius):
                norm =  a.pos - b.pos
                norm /= abs(norm)
                bproj = b.velocity.proj(norm)
                bproj /= abs(bproj)
                aproj = a.velocity.proj(norm)
                aproj /= abs(aproj)
                if not (abs(bproj - norm) < 0.0001 or abs(aproj + norm) < 0.0001):
                    continue
                olda = vector(a.velocity)
                oldb = vector(b.velocity)
                a.velocity += -a.velocity.proj(norm)
                b.velocity += -b.velocity.proj(norm)
                a.velocity += oldb.proj(norm)
                b.velocity += olda.proj(norm)
    for ball in balls:
        ball.arr.axis = vscale * ball.velocity
        ball.arr.pos = ball.pos
    t = t + deltat
    rate(100)
